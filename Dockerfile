FROM ubuntu:23.10

ARG WORK_DIR
ARG INSTALL_DIR
ARG GEANT4_VERSION

RUN apt-get update -qq && \
    apt-get install -y -qq \
    apt-utils \ 
    git \
    wget \
    unzip \
    build-essential \
    freeglut3-dev \
    libboost-all-dev \
    qtbase5-dev \
    libqt5opengl5-dev \
    mercurial \
    libeigen3-dev \
    libsqlite3-dev \
    nlohmann-json3-dev \
    libexpat1-dev \
    libxerces-c-dev \
    libhdf5-dev \
    libhepmc3-dev \
    cmake \
    libssl-dev && \
    apt-get clean all

WORKDIR $WORK_DIR

RUN pwd && \
    ls  && \
    wget https://atlas-vp1.web.cern.ch/atlas-vp1/sources/coin-4.0.0-src.zip && \
    unzip coin-4.0.0-src.zip -d coin-sources && \
    mv coin-sources/* coin && \
    mkdir build_coin && \
    cd build_coin && \
    cmake -DCMAKE_BUILD_TYPE=Debug -DCMAKE_INSTALL_PREFIX=$INSTALL_DIR ../coin && \
    make -j8 && \
    make install 

RUN pwd; ls && \
    wget https://atlas-vp1.web.cern.ch/atlas-vp1/sources/soqt.zip && \
    unzip soqt.zip && \
    mkdir build_soqt && \
    cd build_soqt && \
    cmake -DCMAKE_BUILD_TYPE=Debug -DCMAKE_INSTALL_PREFIX=$INSTALL_DIR ../soqt && \
    make -j8 && \
    make install && \
    pwd; ls  && \
    ls ../ && \
    ls ../../ 
 

RUN pwd && \
    git clone https://gitlab.cern.ch/geant4/geant4.git && \
    cd geant4 && \
    git checkout tags/$GEANT4_VERSION && \
    mkdir build && \
    cd build && \
    rm -rf * && \
    cmake -DCMAKE_INSTALL_PREFIX=$INSTALL_DIR -DGEANT4_INSTALL_DATA=ON -DGEANT4_USE_GDML=ON -DGEANT4_BUILD_MULTITHREADED=ON  ../ && \
    make -j8 && \
    make install 

